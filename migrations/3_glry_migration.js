const BigNumber = require("bignumber.js");

const GLRY = artifacts.require("GLRY");

module.exports = function (deployer) {
    const cap = new BigNumber(30000);
    deployer.deploy(GLRY, cap.shiftedBy(18));
};