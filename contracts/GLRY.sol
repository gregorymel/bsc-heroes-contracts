// contracts/TKN.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Capped.sol";

contract GLRY is ERC20Capped {
    constructor(uint256 cap)
        ERC20Capped(cap) 
        ERC20("GLRY", "GLRY")
    {}

    function withdraw(address to, uint256 amount) public {
        _mint(to, amount);
    }

    function deposit(uint256 amount) public {
        _burn(_msgSender(), amount);
    }
}