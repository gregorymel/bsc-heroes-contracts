require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');
const fs = require('fs');
const privateKey = fs.readFileSync(".secret").toString().trim();
const Web3 = require('web3');

const owner = process.env.NFT_CONTRACT_OWNER;
const provider = new HDWalletProvider(privateKey, process.env.BSC_URL);
const web3 = new Web3(provider);

async function sign(msg, owner) {
    const hash = web3.utils.sha3(msg);
    const signature = await web3.eth.sign(hash, owner);

    return signature;
}

async function verify(msg, signature) {
    const hash = web3.utils.sha3(msg);
    const addr = web3.eth.accounts.recover(hash, signature);
    
    return addr;
}

async function main() {
    const msg = "test_string";
    const signature = await sign(msg, owner);

    const addr = await verify(msg, signature);
    console.log(addr == owner);
}


main()
    .catch(err => console.log(err));