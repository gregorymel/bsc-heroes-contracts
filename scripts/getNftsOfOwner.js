require('dotenv').config();

const Moralis  = require('moralis/node');

const serverUrl = process.env.MORALIS_SERVER_URL;
const appId = process.env.MORALIS_APP_ID;
Moralis.start({ serverUrl, appId });


async function getNfts(playerAddr, nftContractAddr) {
    const options = { chain: 'bsc testnet', address: playerAddr, token_address: nftContractAddr };
    const bscNFTs = await Moralis.Web3API.account.getNFTsForContract(options);

    console.log(bscNFTs);
}

async function main() {
    const nftAddress = process.env.NFT_CONTRACT_ADDR;
    const owner = process.env.NFT_CONTRACT_OWNER;

    await getNfts(owner, nftAddress);
}

main()
    .catch(err => console.log(err));