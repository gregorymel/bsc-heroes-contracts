require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');
const fs = require('fs');
const privateKey = fs.readFileSync(".secret").toString().trim();
const Web3 = require('web3');
const nftAbi = require('./abi/InfiniteHeroAbi.json');

const nftContractAddr = process.env.NFT_CONTRACT_ADDR;
const owner = process.env.NFT_CONTRACT_OWNER;
const provider = new HDWalletProvider(privateKey, process.env.BSC_URL);
const web3 = new Web3(provider);

async function mintNft(web3, player, uri) {
    const contract = new web3.eth.Contract(nftAbi, nftContractAddr);
    await contract.methods.awardItem(player, uri).send({ from: owner });
}

async function main() {
    const uri = "test_nft/1";
    await mintNft(web3, owner, uri);

    const contract = new web3.eth.Contract(nftAbi, nftContractAddr);
    const result = await contract.methods.balanceOf(owner).call();
    console.log("🚀 ~ file: mintNft.js ~ line 25 ~ main ~ result", result);
}

main()
    .catch(err => console.log(err));