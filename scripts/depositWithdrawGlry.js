require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');
const fs = require('fs');
const BigNumber = require("bignumber.js");
const privateKey = fs.readFileSync(".secret").toString().trim();
const Web3 = require('web3');
const tknAbi = require('./abi/GLRY.json');

const tknContractAddr = process.env.TKN_CONTRACT_ADDR;
const owner = process.env.NFT_CONTRACT_OWNER;
const provider = new HDWalletProvider(privateKey, process.env.BSC_URL);
const web3 = new Web3(provider);

async function withdraw(web3, player, amount) {
    const contract = new web3.eth.Contract(tknAbi, tknContractAddr);
    await contract.methods.withdraw(player, amount).send({ from: player });
}

async function deposit(web3, player, amount) {
    const contract = new web3.eth.Contract(tknAbi, tknContractAddr);
    await contract.methods.deposit(amount).send({ from: player });
}

async function main() {
    const amount = new BigNumber(10).shiftedBy(18);
    await withdraw(web3, owner, amount);

    const contract = new web3.eth.Contract(tknAbi, tknContractAddr);
    const result = await contract.methods.balanceOf(owner).call();
    console.log("🚀 ~ file: dipositWithdrawGlry.js ~ line 26 ~ main ~ result", result);

    await deposit(web3, owner, amount);
    const result1 = await contract.methods.balanceOf(owner).call();
    console.log("🚀 ~ file: dipositWithdrawGlry.js ~ line 35 ~ main ~ result1", result1);
}

main()
    .catch(err => console.log(err));